from django.db import models
from main_app.models import Movie


class MovieNight(models.Model):
    date = models.DateField()
    choice_1 = models.ForeignKey(Movie, blank=True, related_name="choice_1", null=True, on_delete=models.SET_NULL)
    choice_2 = models.ForeignKey(Movie, blank=True, related_name="choice_2", null=True, on_delete=models.SET_NULL)
    choice_3 = models.ForeignKey(Movie, blank=True, related_name="choice_3", null=True, on_delete=models.SET_NULL)
    choice_4 = models.ForeignKey(Movie, blank=True, related_name="choice_4", null=True, on_delete=models.SET_NULL)
    choice_5 = models.ForeignKey(Movie, blank=True, related_name="choice_5", null=True, on_delete=models.SET_NULL)
    active = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'main_app_movie_night'
        ordering = ['-date']
        verbose_name = "Night"
        verbose_name_plural = "Nights"

    def __str__(self):
        return str(self.date)
