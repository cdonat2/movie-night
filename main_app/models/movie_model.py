from django.db import models


class Movie(models.Model):
    title = models.CharField(max_length=256, unique=True)
    information_url = models.URLField()
    image_url = models.URLField()
    runtime = models.IntegerField()
    proposed_at = models.DateField(null=True, blank=True)
    chosen = models.BooleanField()
    vetoed = models.BooleanField()
    watched = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'main_app_movie'
        ordering = ['-created_at']
        verbose_name = "movie"
        verbose_name_plural = "movies"

    def __str__(self):
        return self.title
