from django.db import models
from django.contrib.auth.models import User


class Poll(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    choice_mon = models.BooleanField()
    choice_tue = models.BooleanField()
    choice_wed = models.BooleanField()
    choice_thu = models.BooleanField()
    choice_fri = models.BooleanField()
    choice_sat = models.BooleanField()
    choice_sun = models.BooleanField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'main_app_poll'
        ordering = ['-created_at']
        verbose_name = "poll"
        verbose_name_plural = "polls"

    def __str__(self):
        return self.user.username
