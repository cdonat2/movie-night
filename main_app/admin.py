from django.contrib import admin
from main_app.models.poll_model import Poll
from main_app.models.movie_model import Movie
from main_app.models.movie_night_model import MovieNight

# Register your models here.
admin.site.register(Poll)
admin.site.register(Movie)


@admin.register(MovieNight)
class MovieNightAdmin(admin.ModelAdmin):
    list_display = ("date", "choice_1", "choice_2", "choice_3", "choice_4", "choice_5", "active")
    list_filter = ("active",)
