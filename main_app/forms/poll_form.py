from django.forms import ModelForm
from main_app.models.poll_model import Poll


class PollForm(ModelForm):
    class Meta:
        model = Poll
        fields = [
                  'choice_mon',
                  'choice_tue',
                  'choice_wed',
                  'choice_thu',
                  'choice_fri',
                  'choice_sat',
                  'choice_sun'
                  ]

    def __init__(self, *args, **kwargs):
        super(PollForm, self).__init__(*args, **kwargs)
        self.fields['choice_mon'].widget.attrs.update({'class': 'custom_checkbox'})
        self.fields['choice_tue'].widget.attrs.update({'class': 'custom_checkbox'})
        self.fields['choice_wed'].widget.attrs.update({'class': 'custom_checkbox'})
        self.fields['choice_thu'].widget.attrs.update({'class': 'custom_checkbox'})
        self.fields['choice_fri'].widget.attrs.update({'class': 'custom_checkbox'})
        self.fields['choice_sat'].widget.attrs.update({'class': 'custom_checkbox'})
        self.fields['choice_sun'].widget.attrs.update({'class': 'custom_checkbox'})
