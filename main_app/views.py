from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.generic import ListView
from main_app.models.poll_model import Poll
from main_app.forms.poll_form import PollForm
from django.forms import modelformset_factory
from main_app.models.movie_night_model import MovieNight
from django.contrib.auth.decorators import login_required
from main_app.forms.login_form import LoginForm


# Create your views here.
def password_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            password = form.cleaned_data["password"]
            user = authenticate(username="user", password=password)
            if user:
                login(request, user)
                return(redirect('/movies/'))
            else:
                return render(request, 'password_login.html', {'form': form}) # TODO: file location
        else:
            return render(request, 'password_login.html', {'form': form})
    else:
        form = LoginForm()

    return render(request, 'password_login.html', {'form': form})


class MovieView(LoginRequiredMixin, ListView):
    model = MovieNight

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        active_movie_night = MovieNight.objects.filter(active=True).first()
        active_movie_list = [
            active_movie_night.choice_1,
            active_movie_night.choice_2,
            active_movie_night.choice_3,
            active_movie_night.choice_4,
            active_movie_night.choice_5,
            ]

        context['active_movie_list'] = active_movie_list

        past_movie_night_list = MovieNight.objects.filter(active=False)
        past_movie_list = []
        for past_movie_night in past_movie_night_list:
            movie_list = [
                past_movie_night.date,
                past_movie_night.choice_1,
                past_movie_night.choice_2,
                past_movie_night.choice_3,
                past_movie_night.choice_4,
                past_movie_night.choice_5,
                ]
            past_movie_list.append(movie_list)

        context['past_movie_list'] = past_movie_list

        return context


@login_required
def poll_view(request):
    """ Poll View """
    poll_form_set = modelformset_factory(Poll, form=PollForm, extra=0)
    if request.method == "POST":
        formset = poll_form_set(request.POST, request.FILES)
        if formset.is_valid():
            formset.save()
            return HttpResponseRedirect('')
    else:
        formset = poll_form_set()

    context = {'formset': formset}

    return render(request, 'main_app/poll.html', context)

@login_required
def rules_view(request):
    """ Rules View """
    return render(request, 'main_app/rules.html')
