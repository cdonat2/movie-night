from django.urls import path
from .views import MovieView, poll_view, password_login, rules_view
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', password_login),
    #path('', auth_views.LoginView.as_view(next_page="movies/")),
    path('logout/', auth_views.LogoutView.as_view(next_page="/")),
    path('movies/', MovieView.as_view()),
    path('poll/', poll_view),
    path('rules/', rules_view),
]
