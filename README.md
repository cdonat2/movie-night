# README

## Project Idea
This project and website was created to organise a weekly movie night between some friends since 2011.

Python, django, nginx, and gunicorn were used to create a website with a postgreSQL database.

### Movie Overview View:
The main view shows the movie selection of the current week as well as the selections of the last weeks.
![image](images/movies.PNG)

### Voting View:
The voting page is used to find a day when everyone has time.
![image](images/voting.PNG)

The website [film-abend.com](https://www.film-abend.com) is password protected, but you can write to ```c.donat.de@gmail.com``` if you would like access.